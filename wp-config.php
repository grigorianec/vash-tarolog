<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'cards');

/** Имя пользователя MySQL */
define('DB_USER', 'arkada_admin');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'stargate');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4%_^DMP m#,Z.O |XVy28@c*i[EOK95iD(rZr(nBgxS]0|75;w>B!)oDm3=k>9?i');
define('SECURE_AUTH_KEY',  '1w jB3syw0<4*EFQVOq(A<]k9MvG[|6D8R`UO`>lWlT[L~3>4`rpj?A^W[jS=PE;');
define('LOGGED_IN_KEY',    'tJ$=Y4JoLM72=-QDop)*0Bx@c}a|5m C HxI,VzW$+jreb3kbJ7_AJb,S*:,#E4>');
define('NONCE_KEY',        'LL?@RasJCGeIcxsdaH!IBrT3qz$3VG3nt7:95nWrU{Iombeb,RCu2slT~D]]9>&k');
define('AUTH_SALT',        'FVF9{-`(;0?,i}2v{XK_ ezJSF#RJSvr&Fn{},Y/2T$ql4s7.Xm?W|<@pG|RRxzg');
define('SECURE_AUTH_SALT', 'BM8~{h&=u~HEH7fZn&V>fMB~n.O-lh)T|><[,sxrWug03CbIh:8eMo)djr+!U5Ru');
define('LOGGED_IN_SALT',   'neK%,a:/cvmQ]JC0~9-xALR%>Cxl2RIHm`$rS?aihXZSS+MV+v68M7]sCj9zFj;r');
define('NONCE_SALT',       '-i]X3` O0vS:?q/a[PFtBVwSny#R1X qZ!%,^4>^04}}8!w#K.IjT>Vmf5*HyEJs');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
