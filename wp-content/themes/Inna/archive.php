<?php
/*
Template Name: Мой шаблон страницы
*/
?>

<?php get_header();?>
<main class="main">
	<div class="wrap">
		<?php  if (have_posts()) : while(have_posts()) : the_post();?>
			<article class="postItem">
				<div class="post-pic">
					<a href="<?php the_permalink() ?>">
						<?php the_post_thumbnail('medium') ?>
					</a>
				</div>
				<h3 class="postTitle">
					<a href="<?php the_permalink() ?>">
						<?php the_title() ?>
					</a>
				</h3>
				<p>
					Опубликовано <?php the_author();?>
					<span class="post-date"><?php echo get_the_date();?></span>
				</p>
				<p>
					<?php the_field('intro') ?>
					<?php the_excerpt()?>
				</p>
			</article>
		<?php endwhile; ?>
			<?php the_posts_pagination(array(
				'show_all'     => false, // показаны все страницы участвующие в пагинации
				'end_size'     => 1,     // количество страниц на концах
				'mid_size'     => 1,     // количество страниц вокруг текущей
				'type'    => 'list',
			))?>
		<?php else: ?>
			<p>Пост не найден</p>
		<?php endif; ?>
	</div>
	<?php get_sidebar('left_sidebar');?>
</main>
<?php get_footer();?>


