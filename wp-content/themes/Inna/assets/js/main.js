/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

( function( $ ) {

    $('.btn-menu, .mobile-menu').click(function(){
        $('.mobile-menu').addClass('menu-opened');
    });

    $(document).click(function(event) {
        if ($(event.target).closest(".btn-menu").length ) return;
        $('.mobile-menu').removeClass('menu-opened');
        event.stopPropagation();
    });


    $('.feedback-btn__link').fancybox();

    $('.your-class').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: true,
        autoplay: false,
        autoplaySpeed: 1000,
        centerPadding: '0px',
        prevArrow:'<button type="button" class="slick-prev"></button>',
        nextArrow:'<button type="button" class="slick-next"></button>',
        focusOnSelect: true,
        variableWidth: true,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    centerMode: true,
                }
            }
        ]
    });

} )( jQuery );


