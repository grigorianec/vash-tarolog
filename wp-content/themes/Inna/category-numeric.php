<?php get_header()?>
    <div class="container">
        <main class="main">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-9">
                    <?php $posts = get_posts( array(
                        'numberposts' => 0,
                        'category_name'    => "numeric",
                        'orderby'     => 'date',
                        'order'       => 'DESC',
                        'post_type'   => 'post',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ) );

                    foreach( $posts as $post ){
                        setup_postdata($post);
                        ?>
                        <article class="postItem">
                            <div>
                                <a href="<?php the_permalink() ?>">
                                    <?php the_post_thumbnail('medium') ?>
                                </a>
                            </div>
                            <h3 class="postTitle">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </h3>
                            <p>
                                Опубликовано <?php the_author();?>
                                <span class="post-date"><?php  the_date();?></span>
                            </p>
                            <p>
                                <?php the_field('intro') ?>
                                <?php the_excerpt()?>
                            </p>
                        </article>
                        <?php
                    }
                    wp_reset_postdata();?>
                </div>
                <div class="col-3">
                    <?php get_sidebar('left_sidebar');?>
                </div>
            </div>
        </main>
    </div>
<?php get_footer()?>


