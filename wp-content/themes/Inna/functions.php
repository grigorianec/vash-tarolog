<?php

    // правильный способ подключить стили и скрипты
    add_action( 'wp_enqueue_scripts', 'my_styles' );
    add_action( 'wp_enqueue_scripts', 'my_scripts' );
    add_action( 'after_setup_theme', 'card_after_setup' );
    add_action( 'widgets_init', 'bar_widgets' );
	add_action( 'init', 'register_post_types' );
    add_filter('widget_text', 'do_shortcode');
    add_shortcode('my_posts', 'my_posts');
    add_filter('show_admin_bar', '__return_false');

    add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
    function my_navigation_template(){

        return '
        <nav class="navigation %1$s" role="navigation">
            <div class="nav-links">%3$s</div>
        </nav>    
        ';
    }

    the_posts_pagination( array(
        'end_size' => 2,
    ) );


function my_styles() {
        wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
        wp_enqueue_style( 'media-style', get_template_directory_uri() . '/assets/css/media.css' );
        wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/fonts.css' );
	    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css' );
	    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick-theme.css' );
        wp_enqueue_style('fancybox', get_template_directory_uri() . '/fancybox/jquery.fancybox.css');
	    wp_enqueue_style( 'main-style', get_stylesheet_uri() );
    }

    function my_scripts() {
        wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/bootstrap.min.js',
            [], null, true);
        wp_enqueue_script('jquery-3', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js',
            [], null, true);
        wp_enqueue_script('maskedinput', get_template_directory_uri() . '/assets/js/jquery.maskedinput.min.js',
            array('jquery'), null, true);
        wp_enqueue_script('wow', get_template_directory_uri() . '/assets/js/wow.min.js',
            array('jquery'), null, true);
        wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js',
            array('jquery'), null, true);
        wp_enqueue_script('my_fancybox', get_template_directory_uri() . '/fancybox/jquery.fancybox.pack.js',
	        array('jquery-3'), '','true');
	    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js',
		    [], null, true);
    }

    function card_after_setup(){
        register_nav_menu('top','Шапка');
        register_nav_menu('under-top','Под шапкой');
        register_nav_menu('mobile','Мобильное меню');

        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');
        add_theme_support('custom-logo',[
            'width' => '50',
            'height' => '50'
        ]);
        add_theme_support('custom-background',[
            'default-color' => 'ffffff',
            'default-image' => get_template_directory_uri() . '/assets/img/snow.jpg'
        ]);
        
    }

/**
 * @return string
 */


function register_post_types(){
	register_post_type('data', array(
		'labels' => array(
			'name'               => 'Мои данные', // основное название для типа записи
			'singular_name'      => 'Данные', // название для одной записи этого типа
			'add_new'            => 'Добавить', // для добавления новой записи
			'add_new_item'       => 'Добавление инфы', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование', // для редактирования типа записи
			'new_item'           => 'Новое запись', // текст новой записи
			'view_item'          => 'Смотреть запись', // для просмотра записи этого типа.
			'search_items'       => 'Искать данные', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Мои данные', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => null, // зависит от public
		'exclude_from_search' => null, // зависит от public
		'show_ui'             => null, // зависит от public
		'show_in_menu'        => null, // показывать ли в меню адмнки
		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => null, // зависит от public
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 25,
		'menu_icon'           => 'dashicons-format-aside',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => true,
	) );

	register_post_type('backfeeds', array(
		'labels' => array(
			'name'               => 'Отзывы', // основное название для типа записи
			'singular_name'      => 'Отзыв', // название для одной записи этого типа
			'add_new'            => 'Добавить', // для добавления новой записи
			'add_new_item'       => 'Добавление отзыва', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование', // для редактирования типа записи
			'new_item'           => 'Новое запись', // текст новой записи
			'view_item'          => 'Смотреть запись', // для просмотра записи этого типа.
			'search_items'       => 'Искать данные', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Отзывы', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => null, // зависит от public
		'exclude_from_search' => null, // зависит от public
		'show_ui'             => null, // зависит от public
		'show_in_menu'        => null, // показывать ли в меню адмнки
		'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => null, // зависит от public
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 25,
		'menu_icon'           => 'dashicons-format-quote',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => true,
	) );

//		register_taxonomy('days', array('data'), array(
//			'labels'                => array(
//				'name'              => 'Дни',
//				'singular_name'     => 'День',
// 			    'search_items'      => 'день',
//				'all_items'         => 'все дни',
//				'view_item '        => 'Посмотреть день',
//				'edit_item'         => 'Редактировать день',
//				'update_item'       => 'Обновить день',
//				'add_new_item'      => 'Добавить новый день',
//				'new_item_name'     => 'Добавить новый',
//				'menu_name'         => 'Добавления дня консультации',
//			),
//			'description'           => '', // описание таксономии
//			'public'                => true,
//			'hierarchical'          => false,
//		) );

}

function my_posts(){
    $str = '';

    $args = array(
        'numberposts' => 3,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post'
    );

    $posts = get_posts($args);
    global $post;

    foreach($posts as $post){
        setup_postdata($post);

        $link = get_the_permalink();
        $title = get_the_title();
        $dt = get_the_date();
        $intro = get_field('intro');
        $excerpt = get_the_excerpt();
        $picture = get_the_post_thumbnail('','medium');

        $str .= "<div class=\"postItem\">";
                    if(has_post_thumbnail()){
                        $str .=    "<div>$picture</div>";
					} else {
                        $str .= "<img src='" .get_template_directory_uri() . "/assets/img/leaf2.jpg' alt='foto' width='150' height='150'>";
					 }
        $str .=	"<div><strong>$title</strong></div>
						<div><em>$dt</em></div>
						<p>$intro</p>
						<div>$excerpt</div>
						<a href=\"$link\">Далее...</a>
					</div>";
    }
    the_posts_pagination();
    wp_reset_postdata();

    return $str;

}

function mytheme_comment($comment, $args, $depth) {
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}?>
	<<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>"><?php
	if ( 'div' != $args['style'] ) { ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
	} ?>
    <div class="comment-wrap">
        <div class="comment-author vcard"><?php
        if ( $args['avatar_size'] != 0 ) {
            echo get_avatar( $comment, $args['avatar_size'] );
        }
        printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?>
        </div><?php
        if ( $comment->comment_approved == '0' ) { ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php
        } ?>
        <div class="comment-meta commentmetadata">
            <p><?php
                /* translators: 1: date, 2: time */
                printf(
                    __('%1$s at %2$s'),
                    get_comment_date(),
                    get_comment_time()
                ); ?>
            </p><?php
            edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
        </div>
    </div>

	<?php comment_text(); ?>

	<div class="reply"><?php
	comment_reply_link(
		array_merge(
			$args,
			array(
				'add_below' => $add_below,
				'depth'     => $depth,
				'max_depth' => $args['max_depth']
			)
		)
	); ?>
	</div><?php
	if ( 'div' != $args['style'] ) : ?>
		</div><?php
	endif;
}

    function bar_widgets(){
        register_sidebar([
            'name'          => 'left_sidebar',
            'id'            => "left_sidebar",
            'description'   => 'Левый сайдбар',
            'before_widget' => '<div class="widget %2$s">',
            'after_widget'  => "</div>\n",
        ]);

        register_sidebar([
            'name'          => 'bottom_sidebar',
            'id'            => "bottom_sidebar",
            'description'   => 'Нижний сайдбар',
            'before_widget' => '<div class="widget %2$s">',
            'after_widget'  => "</div>\n",
        ]);
    }