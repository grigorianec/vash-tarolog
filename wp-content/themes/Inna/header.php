<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head();?>
</head>
<body <?php body_class();?>>
<header>
	<?php wp_nav_menu([
		'theme_location' => 'mobile',
		'container' => null,
		'items_wrap' => '<ul class="mobile-menu">%3$s</ul>'
	]);?>
    <div class="container">
        <nav>
            <div class="logo">
                <?php if (has_custom_logo()): the_custom_logo(); ?>
                <?php endif;?>
            </div>
            <?php wp_nav_menu([
                'theme_location' => 'top',
                'container' => null,
                'items_wrap' => '<ul class="top-menu">%3$s</ul>'
            ]);?>
            <div class="btn-menu">
                <span class="first"></span>
                <span class="second"></span>
                <span class="third"></span>
            </div>
        </nav>
    </div>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="main-title-up">
                    <h2 class="main-title">
                        Ваш Таролог
                    </h2>
                </div>
                <div class="consult-links">
                    <p>
                        с открытым сердцем и любовью к людям
                    </p>
                    <?php wp_nav_menu([
                        'theme_location' => 'under-top',
                        'container' => null,
                        'items_wrap' => '<ul class="under-top">%3$s</ul>'
                    ]);?>
                    <div id="feedback" class="feedback">
                        <?php echo do_shortcode('[ninja_form id=3 title="Оставить отзыв"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>