<?php
/*
Template Name: Мой шаблон страницы
*/
?>

<?php get_header();?>
<div class="container">
    <main class="main">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-9">
				<?php //require_once 'slider.php';?>
				<?php echo do_shortcode('[smartslider3 slider=3]')?>
				<?php  if (have_posts()) : while(have_posts()) : the_post();?>
                    <article class="postItem">
                        <div class="post-pic">
                            <a href="<?php the_permalink() ?>">
								<?php the_post_thumbnail('medium') ?>
                            </a>
                        </div>
                        <h3 class="postTitle">
                            <a href="<?php the_permalink() ?>">
								<?php the_title() ?>
                            </a>
                        </h3>
                        <p>
                            Опубликовано <?php the_author();?>
                            <span class="post-date"><?php echo get_the_date();?></span>
                        </p>
                        <p>
							<?php the_field('intro') ?>
							<?php the_excerpt()?>
                        </p>
                    </article>
				<?php endwhile; ?>
					<?php the_posts_pagination(array(
						'show_all'     => false, // показаны все страницы участвующие в пагинации
						'end_size'     => 1,     // количество страниц на концах
						'mid_size'     => 1,     // количество страниц вокруг текущей
						'type'    => 'list',
					))?>
				<?php else: ?>
                    <p>Пост не найден</p>
				<?php endif; ?>
                </div>
                <div class="col-3">
					<?php get_sidebar('left_sidebar');?>
                </div>
            </main>
        </div>
    </div>
</div>
<?php get_footer();?>


