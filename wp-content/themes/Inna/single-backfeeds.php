<?php
/*
* The Template for displaying all single posts.
*/
?>

<?php get_header();?>
    <div class="container">
        <main class="main">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-9">
                        <h2 class="postTitle-single text-center">
                            Отзывы
                        </h2>
                        <div class="your-class">
                        <?php
                        // параметры по умолчанию
                        $arg = array(
                            'numberposts' => 0,
                            'order'  => 'ASC',
                            'post_type' => 'backfeeds',
                            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                        );

                        $posts = get_posts($arg);
                        foreach( $posts as $post ){
                            setup_postdata($post);
                            ?>
                                <div class="pic-name">
                                    <div class="slide-wrap">
                                        <div class="pic-slide">
                                            <?php if(has_post_thumbnail()){
                                                the_post_thumbnail();
                                            }else{
                                                echo "<img src='" .get_template_directory_uri() . "/assets/img/not_image.jpg' alt='foto'>";
                                            }
                                            ;?>
                                            <div class="backfeed-name">
                                                <?php the_field('name')?>
                                            </div>
                                        </div>
                                        <div class="content-reviews">
                                            <?php the_content()?>
                                        </div>
                                    </div>
                                </div>

                            <?php
                                 }
                                wp_reset_postdata(); // сброс
                            ?>
                        </div>
                        <div class="feedback-btn">
                            <a class="feedback-btn__link" href="#feedback">
                                Оставить отзыв
                            </a>
                        </div>
                        <div class="home">
                            <a class="home-url" href="<?php echo home_url('/')?>">
                                Назад
                            </a>
                        </div>
                    </div>
                    <div class="col-3">
                        <?php get_sidebar('left_sidebar');?>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php get_footer();?>

