<?php
/*
* The Template for displaying all single posts.
*/
?>


<?php get_header();?>
    <div class="container">
        <main class="main">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-9">
                    <?php while(have_posts()): the_post(); ?>
                        <article class="postItem">
                           <div class="post-pic">
                               <?php the_post_thumbnail('medium') ?>
                           </div>
                            <h2 class="postTitle-single">
                                <?php the_title() ?>
                            </h2>
                            <?php if(term_exists( 'days' )):?>
                                <?php $days = get_the_terms($post->ID,'days')?>
                                <div>
                                    <strong>
                                        День консультации:
                                    </strong>
                                    <?php foreach ($days as $day) : ?>
                                        <a href="<?php echo get_term_link($day->term_id)?>">
                                            <?= $day->name?>
                                        </a>
                                    <?php endforeach;?>
                                </div>
                            <?php endif;?>
                            <p>
                                Опубликовано <?php the_author();?>
                                <span class="post-date"><?php  the_date();?></span>
                            </p>
                            <p>
                                <?php the_content()?>
                            </p>
                        </article>
                    <?php endwhile; ?>
                    <a href="<?php echo home_url('/')?>">
                        Назад
                    </a>
                </div>
                <div class="col-lg-3">
                    <?php get_sidebar('left_sidebar');?>
                </div>
            </div>
        </main>
    </div>
<?php get_footer();?>


