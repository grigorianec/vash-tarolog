<?php
/*
* The Template for displaying all single posts.
*/
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head();?>
</head>
<body>
<?php get_header();?>
    <div class="container">
        <main class="main">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-9">
                    <?php while(have_posts()): the_post(); ?>
                        <article class="postItem">
                           <div class="post-pic">
                               <?php the_post_thumbnail('medium') ?>
                           </div>
                            <h2 class="postTitle-single">
                                <?php the_title() ?>
                            </h2>
                            <p>
                                Опубликовано <?php the_author();?>
                                <span class="post-date"><?php echo get_the_date();?></span>
                            </p>
                            <p>
                                <?php the_content()?>
                            </p>
                            <p>
                                <?php comments_template()?>
                            </p>
                        </article>
                    <?php endwhile; ?>
                    <a href="<?php echo home_url('/')?>">
                        Назад
                    </a>
                </div>
                <div class="col-3">
                    <?php get_sidebar('left_sidebar');?>
                </div>
            </div>
        </main>
    </div>
<?php get_footer();?>
</body>
</html>

